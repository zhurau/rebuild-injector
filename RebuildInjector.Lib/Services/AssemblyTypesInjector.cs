﻿using System.Reflection;
using System.Runtime.InteropServices;

namespace RebuildInjector.Lib.Services
{
    internal class AssemblyTypesInjector
    {
        private readonly IServiceProvider _serviceProvider;

        public AssemblyTypesInjector(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Injects types defined in the <paramref name="assembly"/> into current application.
        /// </summary>
        /// <returns>Dictionary of objects successfully injected.</returns>
        public Dictionary<Type, object> Inject(
            Assembly assembly,
            Action<Type, string> skippedTypeInjectionHandler)
        {
            var recompiledTypes = assembly.GetTypes();
            var recompiledTypesAndObjects = recompiledTypes.ToDictionary(KeySelector, ValueSelector);

            var app = AppDomain.CurrentDomain;
            var recompiledTypesAnalogues = app
                .GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => recompiledTypes.Contains(x))
                .Distinct();

            var instancesOfExistingTypes = recompiledTypesAnalogues
                .Select(x =>
                {
                    var obj = _serviceProvider.GetService(x);
                    if (obj is null)
                    {
                        skippedTypeInjectionHandler.Invoke(x, "not found in service provider");
                    }

                    return (type: x, obj);
                })
                .Where(x => x.obj != null)
                .ToArray()
                .ToDictionary(x => x.type, x => x.obj);

            foreach (var pair in instancesOfExistingTypes)
            {
                var type = pair.Key;
                var newObj = Activate(type);
                Replace(pair.Value, newObj, type);
            }

            return instancesOfExistingTypes;
        }

        private static Func<Type, Type> KeySelector => (t) =>
        {
            var baseType = t;
            var @interface = t.GetInterfaces().FirstOrDefault();

            return @interface ?? t.BaseType ?? baseType;
        };

        private static Func<Type, object> ValueSelector => Activate;

        private static object Activate(Type t) => Activator.CreateInstance(t);

        private static void Replace(object currentObj, object newVal, Type t)
        {
            if (currentObj == null)
            {
                throw new ArgumentNullException(nameof(currentObj));
            }
            
            if (newVal == null)
            {
                throw new ArgumentNullException(nameof(newVal));
            }

            var size = Marshal.SizeOf(t);
            var ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(newVal, ptr, false);
            Marshal.PtrToStructure(ptr, currentObj);
            Marshal.FreeHGlobal(ptr);
        }
    }
}
