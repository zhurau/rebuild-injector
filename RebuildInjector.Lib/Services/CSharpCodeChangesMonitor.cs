﻿namespace RebuildInjector.Lib.Services
{
    public class CSharpCodeChangesMonitor
    {
        private const string CSharpFileExtension = "*.cs";
        private FileSystemWatcher _watcher;

        public event Action<string> ScriptUpdated = delegate { };

        public void SetPath(Uri path)
        {
            _watcher = new FileSystemWatcher
            {
                NotifyFilter= NotifyFilters.FileName | NotifyFilters.LastWrite,
                Path = path.AbsolutePath,
                EnableRaisingEvents = true,
                Filter = CSharpFileExtension,
                IncludeSubdirectories= true,
            };

            _watcher.Changed += OnScriptUpdated;
        }

        private void OnScriptUpdated(object sender, FileSystemEventArgs e)
        {
            ScriptUpdated.Invoke(e.FullPath);
        }
    }
}
