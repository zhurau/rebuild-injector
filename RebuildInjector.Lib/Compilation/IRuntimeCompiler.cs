﻿using System.Reflection;

namespace RebuildInjector.Lib.Compilation
{
    public interface IRuntimeCompiler
    {
        Assembly Compile(string fileName);
    }
}
