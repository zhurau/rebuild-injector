﻿using System.Reflection;
using Microsoft.CodeDom.Providers.DotNetCompilerPlatform;

namespace RebuildInjector.Lib.Compilation
{
    internal class NetFrameworkRuntimeCompiler : IRuntimeCompiler
    {
        private const string OutputAssemblyNamePrefix = "DynamicallyCompiled";

        private readonly CSharpCodeProvider _codeProvider;

        public NetFrameworkRuntimeCompiler(CSharpCodeProvider codeProvider)
        {
            _codeProvider = codeProvider;
        }

        public Assembly Compile(string fileName)
        {
            var compileParameters = new System.CodeDom.Compiler.CompilerParameters
            {
                GenerateInMemory = true,
                GenerateExecutable = false,
                IncludeDebugInformation = false,
                // OutputAssembly = $"{OutputAssemblyNamePrefix}-{Guid.NewGuid()}", // GetOutputAssemblyPath(),
            };

            return _codeProvider
                .CompileAssemblyFromFile(compileParameters, fileName)
                .CompiledAssembly;
        }

        private string GetOutputAssemblyPath()
        {
            var assemblyId = Guid.NewGuid().ToString();
            var assemblyName = $"{OutputAssemblyNamePrefix}-{assemblyId}";
            var currentExecutablePath = AppContext.BaseDirectory;
            return Path.Combine(currentExecutablePath, assemblyName);
        }
    }
}
