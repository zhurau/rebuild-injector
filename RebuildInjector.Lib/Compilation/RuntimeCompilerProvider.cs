﻿using System.Runtime.InteropServices;

namespace RebuildInjector.Lib.Compilation
{
    public interface IRuntimeCompilerProvider
    {
        IRuntimeCompiler Get();
    }

    public class RuntimeCompilerProvider : IRuntimeCompilerProvider
    {
        private readonly IRuntimeCompiler _frameworkRuntimeCompiler;
        private readonly IRuntimeCompiler _coreRuntimeCompiler;

        public RuntimeCompilerProvider(
            IRuntimeCompiler frameworkRuntimeCompiler,
            IRuntimeCompiler coreRuntimeCompiler)
        {
            _frameworkRuntimeCompiler = frameworkRuntimeCompiler;
            _coreRuntimeCompiler = coreRuntimeCompiler;
        }

        public IRuntimeCompiler Get()
        {
            return IsNetCore
                ? _coreRuntimeCompiler
                : _frameworkRuntimeCompiler;
        }

        private bool IsNetCore =>
            RuntimeInformation.FrameworkDescription.StartsWith(".NET Core", StringComparison.OrdinalIgnoreCase);
    }
}
