﻿using Microsoft.CodeDom.Providers.DotNetCompilerPlatform;
using Unity;
using Unity.Extension;
using RebuildInjector.Lib.Compilation;
using RebuildInjector.Lib.Services;
using RebuildInjector.Lib.ServicesCollection;

namespace RebuildInjector.Lib.Dependencies
{
    public class DependencyCollection : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.RegisterType<CSharpCodeChangesMonitor>();
            Container.RegisterType<AssemblyTypesInjector>();
            Container.RegisterType<IIntValueProvider, IntValueProvider>();
            Container.RegisterType<IStringValueProvider, StringValueProvider>();

            // compilers
            Container.RegisterType<NetCoreRuntimeCompiler>();
            Container.RegisterType<CSharpCodeProvider>();
            Container.RegisterType<NetFrameworkRuntimeCompiler>();
            Container.RegisterFactory<IRuntimeCompilerProvider>(c =>
                new RuntimeCompilerProvider(c.Resolve<NetFrameworkRuntimeCompiler>(), c.Resolve<NetCoreRuntimeCompiler>()));
        }
    }
}
