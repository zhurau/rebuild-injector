﻿namespace RebuildInjector.Lib.ServicesCollection
{
    public interface IStringValueProvider
    {
        string GetStringValue();
    }

    public class StringValueProvider : IStringValueProvider
    {
        public string GetStringValue()
        {
            return nameof(StringValueProvider);
        }
    }
}
