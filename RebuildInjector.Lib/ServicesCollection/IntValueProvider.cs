﻿namespace RebuildInjector.Lib.ServicesCollection
{
    public interface IIntValueProvider
    {
        int GetIntValue();
    }

    public class IntValueProvider : IIntValueProvider
    {
        public int GetIntValue()
        {
            return 512;
        }
    }
}
