﻿namespace RebuildInjector.Lib.ServicesCollection
{
    public class LocalServiceProvider : IServiceProvider
    {
        private readonly Dictionary<Type, object> _services;

        public LocalServiceProvider()
        {
            _services = new Dictionary<Type, object>();
        }

        public LocalServiceProvider(Dictionary<Type, object> services)
        {
            _services = services;
        }

        public void RegisterSingletone(object o)
        {
            _services.Add(o.GetType(), o);
        }

        public void RegisterSingletone<TKey, TService>()
        {
            _services.Add(typeof(TKey), Activator.CreateInstance(typeof(TService)));
        }

        public T GetService<T>()
            where T : class
        {
            return GetService(typeof(T)) as T;
        }

        public object GetService(Type serviceType)
        {
            return _services.ContainsKey(serviceType)
                ? _services[serviceType]
                : null;
        }
    }
}
