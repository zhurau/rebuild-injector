﻿using System;

namespace RebuildInjector.ConsoleNet4.Models
{
    public class ScriptUpdatedArgs : EventArgs
    {
        public string FilePath { get; }

        public ScriptUpdatedArgs(string filePath)
        {
            FilePath = filePath;
        }
    }
}
