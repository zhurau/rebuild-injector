﻿namespace RebuildInjector.ConsoleNet4.ServicesCollection
{
    public interface IIntValueProvider
    {
        int GetIntValue();
    }

    public class IntValueProvider : IIntValueProvider
    {
        public int GetIntValue()
        {
            return 5230;
        }
    }
}
