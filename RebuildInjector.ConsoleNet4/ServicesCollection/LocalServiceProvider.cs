﻿using System;
using System.Collections.Generic;

namespace RebuildInjector.ConsoleNet4.ServicesCollection
{
    internal class LocalServiceProvider : IServiceProvider
    {
        public Dictionary<Type, object> Services { get; }

        public LocalServiceProvider()
        {
            Services = new Dictionary<Type, object>();
        }

        public LocalServiceProvider(Dictionary<Type, object> services)
        {
            Services = services;
        }

        public void RegisterSingletone(object o)
        {
            Services.Add(o.GetType(), o);
        }

        public void RegisterSingletone<TKey, TService>()
        {
            Services.Add(typeof(TKey), Activator.CreateInstance(typeof(TService)));
        }

        public T GetService<T>()
            where T : class
        {
            return GetService(typeof(T)) as T;
        }

        public object GetService(Type serviceType)
        {
            return Services.ContainsKey(serviceType)
                ? Services[serviceType]
                : null;
        }
    }
}
