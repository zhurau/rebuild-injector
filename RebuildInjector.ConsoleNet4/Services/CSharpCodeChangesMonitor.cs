﻿using System;
using System.IO;

namespace RebuildInjector.ConsoleNet4.Services
{
    public class CSharpCodeChangesMonitor
    {
        private const string CSharpFileExtension = "*.cs";
        private FileSystemWatcher _watcher;

        public event Action<string> ScriptUpdated = delegate { };

        public void SetPath(Uri path)
        {
            _watcher = new FileSystemWatcher
            {
                NotifyFilter= NotifyFilters.FileName | NotifyFilters.LastWrite,
                Path = path.AbsolutePath,
                EnableRaisingEvents = true,
                Filter = CSharpFileExtension,
                IncludeSubdirectories= true,
            };

            _watcher.Changed += OnScriptUpdated;

            Console.WriteLine($"Monitoring {path.AbsolutePath} folder.");
        }

        private void OnScriptUpdated(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine($"{nameof(CSharpCodeChangesMonitor)} detected file change {e.FullPath}");
            ScriptUpdated.Invoke(e.FullPath);
        }
    }
}
