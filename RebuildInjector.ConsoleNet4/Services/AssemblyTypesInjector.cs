﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using RebuildInjector.ConsoleNet4.ServicesCollection;

namespace RebuildInjector.ConsoleNet4.Services
{
    internal class AssemblyTypesInjector
    {
        private readonly LocalServiceProvider _serviceProvider;

        public AssemblyTypesInjector(LocalServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Injects types defined in the <paramref name="assembly"/> into current application.
        /// </summary>
        /// <returns>Dictionary of objects successfully injected.</returns>
        public Dictionary<Type, object> Inject(
            Assembly assembly,
            Action<Type, string> skippedTypeInjectionHandler)
        {
            var recompiledTypes = assembly.GetTypes().Select(x => x.FullName);

            var app = AppDomain.CurrentDomain;
            var currentTypes = app
                .GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => !x.IsAbstract && !x.IsInterface);

            var recompiledTypesAnalogues = currentTypes
                .Where(x => recompiledTypes.Contains(x.FullName))
                .Distinct();

            var instancesOfExistingTypes = recompiledTypesAnalogues
                .Select(x =>
                {
                    var obj = _serviceProvider.GetService(x);
                    if (obj is null)
                    {
                        skippedTypeInjectionHandler.Invoke(x, "not found in service provider");
                    }

                    return (type: x, obj);
                })
                .Where(x => x.obj != null)
                .ToArray()
                .ToDictionary(x => x.type, x => x.obj);

            foreach (var pair in instancesOfExistingTypes)
            {
                var type = pair.Key;
                var newObj = Activate(type);
                Replace(pair.Value, newObj, type);

                Console.WriteLine($"{type.FullName} was updated.");
            }

            return instancesOfExistingTypes;
        }

        private static object Activate(Type t) => Activator.CreateInstance(t);

        private static void Replace(object currentObj, object newVal, Type t)
        {
            if (currentObj == null)
            {
                throw new ArgumentNullException(nameof(currentObj));
            }

            if (newVal == null)
            {
                throw new ArgumentNullException(nameof(newVal));
            }

            var size = Marshal.SizeOf(t);
            var ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(newVal, ptr, false);
            Marshal.PtrToStructure(ptr, currentObj);
            Marshal.FreeHGlobal(ptr);
        }
    }
}
