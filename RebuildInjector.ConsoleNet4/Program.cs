﻿using System;
using System.Text;
using Microsoft.CodeDom.Providers.DotNetCompilerPlatform;
using RebuildInjector.ConsoleNet4.Services;
using RebuildInjector.ConsoleNet4.ServicesCollection;
using RebuildInjector.Lib.Compilation;

namespace RebuildInjector.ConsoleNet4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.InputEncoding = Encoding.UTF8;

            // services to rewrite in memory
            var servicesProvider = new LocalServiceProvider();
            servicesProvider.RegisterSingletone<IStringValueProvider, StringValueProvider>();
            servicesProvider.RegisterSingletone(new StringValueProvider());
            servicesProvider.RegisterSingletone<IIntValueProvider, IntValueProvider>();
            servicesProvider.RegisterSingletone(new IntValueProvider());

            var provider = new CSharpCodeProvider();
            var compiler = new RuntimeCompilerProvider(provider).Get();

            var injector = new AssemblyTypesInjector(servicesProvider);

            var appFolder = AppDomain.CurrentDomain.BaseDirectory;
            var monitorFolder = appFolder.Substring(0, appFolder.IndexOf("bin", StringComparison.OrdinalIgnoreCase));
            var monitor = new CSharpCodeChangesMonitor();
            monitor.SetPath(new Uri(monitorFolder));
            monitor.ScriptUpdated += s =>
            {
                var assembly = compiler.Compile(s);
                injector.Inject(assembly, SkippedHandler);
            };

            var str = servicesProvider.GetService<IStringValueProvider>();
            var intt = servicesProvider.GetService<IIntValueProvider>();

            do
            {
                Console.WriteLine(str.GetStringValue());
                Console.WriteLine(intt.GetIntValue());
            }
            while (!(Console.ReadLine() ?? "").StartsWith("s"));
        }

        static void SkippedHandler(Type t, string s)
        {
            Console.WriteLine($"Type {t.Name} was skipped during injection. Reason: " + s);
        }
    }
}