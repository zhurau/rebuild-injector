﻿using RebuildInjector.ProxyObject.Registration.Models;

namespace RebuildInjector.ProxyObject.Registration
{
    public interface IRebuildNotificatorProvider
    {
        INotificator<TService> Observe<TService>(TService service)
            where TService : class;
    }

    internal class RebuildNotificatorProvider : IRebuildNotificatorProvider
    {
        private readonly INotificatorContainer _container;

        public RebuildNotificatorProvider(INotificatorContainer container)
        {
            _container = container;
        }

        public INotificator<TService> Observe<TService>(TService service)
            where TService : class
        {
            var notificator = new Notificator<TService>(service);
            _container.Add(notificator);
            return notificator;
        }
    }
}
