﻿using RebuildInjector.ProxyObject.Registration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RebuildInjector.ProxyObject.Registration
{
    public interface INotificatorContainer
    {
        public void Add(object notificator);

        INotificator<TService> Get<TService>()
            where TService : class;
    }

    internal class NotificatorContainer : INotificatorContainer
    {
        private List<object> notificators = new List<object>();

        public void Add(object notificator)
        {
            ValidateObject(notificator);
            notificators.Add(notificator);
        }

        public INotificator<TService> Get<TService>() where TService : class
        {
            throw new NotImplementedException();
        }

        private static void ValidateObject(object notificator)
        {
            if (InheritsFromNotificator(notificator))
            {
                throw new ArgumentException("Argument does not represent Notificator type", nameof(notificator));
            }
        }

        private static bool InheritsFromNotificator(object o) => o
            .GetType()
            .GetGenericTypeDefinition()
            .IsAssignableTo(typeof(INotificator<>));
    }
}
