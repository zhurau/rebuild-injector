﻿namespace RebuildInjector.ProxyObject.Registration.Models
{
    public interface INotificator<TService>
        where TService : class
    {
        event Action<TService> ServiceUpdated;
    }

    public class Notificator<TService> : INotificator<TService>
        where TService : class
    {
        public event Action<TService> ServiceUpdated;

        public TService Service { get; set; }

        public Notificator(TService service)
        {
            Service = service;
            ServiceUpdated += OnServiceUpdated;
        }

        protected void OnServiceUpdated(TService service)
        {
            Service = service;
        }
    }
}
