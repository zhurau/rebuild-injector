﻿using System.Text;
using Microsoft.CodeDom.Providers.DotNetCompilerPlatform;
using LocalServiceProvider = RebuildInjector.Lib.ServicesCollection.LocalServiceProvider;
using RebuildInjector.Lib.Compilation;
using RebuildInjector.Lib.Services;
using RebuildInjector.ProxyObject.Services;
using RebuildInjector.ProxyObject.ServicesCollection;

namespace RebuildInjector.ProxyObject
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.InputEncoding = Encoding.UTF8;

            // services to rewrite in memory
            var localProvider = RegisterLocalServices();

            var provider = new CSharpCodeProvider();
            var compiler = new RuntimeCompilerProvider(provider).Get();

            var injector = new AssemblyTypesInjector(localProvider);

            var appFolder = @"d:\Documents\Univers\codes\diff\RebuildInjector\RebuildInjector.Lib\"; // AppDomain.CurrentDomain.BaseDirectory;
            var monitor = new CSharpCodeChangesMonitor();
            monitor.SetPath(new Uri(appFolder));
            monitor.ScriptUpdated += s =>
            {
                var assembly = compiler.Compile(s);
                injector.Inject(assembly, SkippedHandler);
            };

            var str = localProvider.GetService<IStringValueProvider>();
            var intt = localProvider.GetService<IIntValueProvider>();

            do
            {
                Console.WriteLine(str.GetStringValue());
                Console.WriteLine(intt.GetIntValue());
            }
            while (!(Console.ReadLine() ?? "").StartsWith("s"));
        }

        private static LocalServiceProvider RegisterLocalServices()
        {
            var servicesProvider = new LocalServiceProvider();
            servicesProvider.RegisterSingletone<IStringValueProvider, StringValueProvider>();
            servicesProvider.RegisterSingletone(new StringValueProvider());
            servicesProvider.RegisterSingletone<IIntValueProvider, IntValueProvider>();
            servicesProvider.RegisterSingletone(new IntValueProvider());
            return servicesProvider;
        }

        static void SkippedHandler(Type t, string s)
        {
            Console.WriteLine($"Type {t.Name} was skipped during injection. Reason: " + s);
        }
    }
}